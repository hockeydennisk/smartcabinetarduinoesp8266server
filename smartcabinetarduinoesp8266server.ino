#include <Servo.h>
#include <SoftwareSerial.h>
#include <PololuLedStrip.h>

#define DEBUG true

// Create an ledStrip object and specify the pin it will use.
PololuLedStrip<2> objectFirstLedStrip;
PololuLedStrip<4> objectSecondLedStrip;

// Create a buffer for holding the colors (3 bytes per color).
#define LED_COUNT 15
rgb_color arrayONELEDColors[LED_COUNT] = {0};
rgb_color arrayTWOLEDColors[LED_COUNT] = {0};
bool bLEDBreathe[LED_COUNT] = {0};

#define OUTSIDER_ONE_LED_PIN 5
#define OUTSIDER_TWO_LED_PIN 6
int brightness = 0;    // how bright the LED is
int fadeAmount = 1;    // how many points to fade the LED by

const String SSID="Hippo.C iPhone";//Wifi名稱
const String PASSWORD="00000000";//Wifi密碼
//const String SSID="HUAWEI-B315-7419";//Wifi名稱
//const String PASSWORD="0224377384";//Wifi密碼

bool bWIFIAndServerReady = false; //指出 Wifi是否已連接，且已將 ESP8266伺服器設定妥當。
bool bOutsideLEDBreathe[2] = {0};
String logText = "";//所有歷史記錄訊息會累積在這。

SoftwareSerial ESP8266SoftwareSerial(10,11); //(RX,TX)

#define SERVO_PIN 9
Servo ServoLock; // 建立Servo物件，控制伺服馬達

void setup() {
  // initialize both serial ports:
  Serial.begin(9600);
  ESP8266SoftwareSerial.begin(9600);
//  ESP8266SoftwareSerial.setTimeout(2000);
//  Serial.println("Set ESP8266 baud rate to 9600 and re-enable...");
//  ESP8266SoftwareSerial.print("AT+CIOBAUD=9600\r\n");  
//  ESP8266SoftwareSerial.end();
//  ESP8266SoftwareSerial.begin(9600);

  pinMode(OUTSIDER_ONE_LED_PIN, OUTPUT);
  pinMode(OUTSIDER_TWO_LED_PIN, OUTPUT);
  
  //WIFI initialize
  WIFIServerInitialize();
}

void loop() {
	delay(100);
	if(bWIFIAndServerReady){//Wifi有連接
//		String sESP8266Response = readESP8266Response(DEBUG);
  String sESP8266Response;
  if(ESP8266SoftwareSerial.available())
  {
    delay(100);
    while(ESP8266SoftwareSerial.available())
    {
      char cAvailableChar = ESP8266SoftwareSerial.read();
      sESP8266Response += cAvailableChar;        
    }
    
    if(DEBUG)
    {
      Serial.print(sESP8266Response);
    } 
    //ESP8226 receive data
    //if(sESP8266Response.indexOf("+IPD,") > -1)
    {
      //Get client ID of sender
      int iClientID = (sESP8266Response.substring(sESP8266Response.indexOf(",")+1,sESP8266Response.indexOf(",")+2)).toInt();
      //Get data 
      String sRoData = sESP8266Response.substring(sESP8266Response.indexOf(":")+1);
      Serial.print("ClientID:");
      Serial.println(iClientID);
      Serial.print("RoData:");
      Serial.println(sRoData);
      //Analysis
      byte cType = (byte)sRoData.charAt(0);
      Serial.print("TYPE:");
      Serial.println(cType);
      
      byte byteIndex;
      byte byteSubIndex;
      byte byteValue;
      unsigned char cSubIndex16;
      switch(cType)
      {
        //Outside LED
        case '0':
          byteIndex = (byte)sRoData.charAt(1);
          byteValue = (byte)sRoData.charAt(2);

          if(byteValue == '0')
          {// sets the LED on
            if(byteIndex == '1')
            {
              digitalWrite(OUTSIDER_ONE_LED_PIN, LOW);
              Serial.println("First outside LED close.");
            }
            else if(byteIndex == '2')
            { 
              digitalWrite(OUTSIDER_TWO_LED_PIN, LOW);
              Serial.println("Second outside LED close.");
            }
            bOutsideLEDBreathe[byteIndex-49] = false;
          }else
          if(byteValue == '1')
          {// sets the LED on
            if(byteIndex == '1')
            {
              digitalWrite(OUTSIDER_ONE_LED_PIN, HIGH);
              Serial.println("First outside LED open.");
            }
            else if(byteIndex == '2')
            {
              digitalWrite(OUTSIDER_TWO_LED_PIN, HIGH);
              Serial.println("Second outside LED open.");
            }
            bOutsideLEDBreathe[byteIndex-49] = false;
          }else
          if(byteValue == '2')
          {
            if(byteIndex == '1')
            {
              // sets the LED breathe
              bOutsideLEDBreathe[0] = true;
              Serial.println("First outside LED breathe.");
            }
            else if(byteIndex == '2')
            {
              // sets the LED breathe
              bOutsideLEDBreathe[1] = true;
              Serial.println("Second outside LED breathe.");
            }
          }
          
          break;
        //insider LED
        case '1':
          byteIndex = (byte)sRoData.charAt(1);
          byteSubIndex = (byte)sRoData.charAt(2);
          byteValue = (byte)sRoData.charAt(3);
          cSubIndex16 = (unsigned char)strtol((char*)&byteSubIndex, NULL, 16);
          if(byteValue == '0')
          {
            // Write the colors to the LED strip.
            if(byteIndex == '1')
            {
              // sets the LED off
              arrayONELEDColors[cSubIndex16-1] = (rgb_color){ 0, 0, 0 };
              objectFirstLedStrip.write(arrayONELEDColors, LED_COUNT);
              Serial.print("First inside LED<"); 
              Serial.print(cSubIndex16);
              Serial.println("> close."); 
            }
            else  if(byteIndex == '2')
            {
              // sets the LED off
              arrayTWOLEDColors[cSubIndex16-1] = (rgb_color){ 0, 0, 0 };
              objectSecondLedStrip.write(arrayTWOLEDColors, LED_COUNT);
              Serial.print("Sencond inside LED<"); 
              Serial.print(cSubIndex16);
              Serial.println("> close."); 
            }
          }else
          if(byteValue == '1')
          {
            // Write the colors to the LED strip.
            if(byteIndex == '1')
            {
              // sets the LED on
              arrayONELEDColors[cSubIndex16-1] = (rgb_color){ 255, 255, 255};
              objectFirstLedStrip.write(arrayONELEDColors, LED_COUNT);  
              Serial.print("First inside LED<"); 
              Serial.print(cSubIndex16);
              Serial.println("> open."); 
            }
            else
            {
              // sets the LED on
              arrayTWOLEDColors[cSubIndex16-1] = (rgb_color){ 255, 255, 255};
              objectSecondLedStrip.write(arrayTWOLEDColors, LED_COUNT);
              Serial.print("Second inside LED<"); 
              Serial.print(cSubIndex16);
              Serial.println("> open."); 
            }
          }else
          if(byteValue == '2')
          {// sets the LED breathe
            //bOutsideLEDBreathe[byteIndex-1] = true;
          }            
          break;
        case '2':
          ServoLock.attach(SERVO_PIN); // 連接數位腳位與伺服馬達的訊號線
          delay(100);
          byteValue = (byte)sRoData.charAt(1);
          if(byteValue == '0')
          {
            ServoLock.write(0);
          }else if(byteValue == '1')
          {
            ServoLock.write(180);
          }
          delay(700);
          ServoLock.detach(); // 斷開數位腳位與伺服馬達的訊號線
          break;
        default:
          break;
      }

      
    }//{+IPD}
  }
    if(bOutsideLEDBreathe[0])
    {
      // set the brightness of pin 9:
      analogWrite(OUTSIDER_ONE_LED_PIN, brightness);   
      // change the brightness for next time through the loop:
      brightness = brightness + fadeAmount;
      // reverse the direction of the fading at the ends of the fade:
      if (brightness == 0 || brightness == 255) {
        fadeAmount = -fadeAmount ;
      }
    }
//    delay(10);
    if(bOutsideLEDBreathe[1])
    {
      // set the brightness of pin 9:
      analogWrite(OUTSIDER_TWO_LED_PIN, brightness);   
      // change the brightness for next time through the loop:
      brightness = brightness + fadeAmount;
      // reverse the direction of the fading at the ends of the fade:
      if (brightness == 0 || brightness == 255) {
        fadeAmount = -fadeAmount ;
      }
    }
//    proxyToESP8266();
	}else{
	  delay(2000);
		WIFIServerInitialize();
	}	
}

//連線到 Wifi，並將 ESP8266設置為伺服器模式。
//成功則傳回 true，否則傳回 false。
bool WIFIServerInitialize(){
  bWIFIAndServerReady = false;
  String sResponse="";
  String sCommand="";
	log("Contacting ESP8266...");
  sCommand = String("AT");
	sResponse = sendCommandToESP8226(sCommand, 1000, DEBUG);
	// 成功連線到 ESP8266
  if(sResponse.indexOf("OK") > -1)
  {
		log("Got response from ESP8266.");
    log("Set AP+STA mode.");
    sCommand = String("AT+CWMODE=3");
    sResponse = sendCommandToESP8226(sCommand, 1000, DEBUG);

    log("Check wifi status...");
    sCommand = String("AT+CWJAP?");
    sResponse = sendCommandToESP8226(sCommand, 1000, DEBUG);
    if(sResponse.indexOf("No AP") > -1)
    {
		  log("Connecting to wifi...");
CONNECTWIFI:
      sCommand = String("AT+CWJAP=\""+SSID+"\",\""+PASSWORD+"\"");
  		sResponse = sendCommandToESP8226(sCommand, 7000, DEBUG);
      if((sResponse.indexOf("WIFI CONNECTED") > -1) ||
      (sResponse.indexOf("GOT WIFI") > -1) || (sResponse.indexOf(SSID) > -1 ))
      {
        log("Wifi connected.");
      }
      else
      {
       log("ERROR: Failed to connect to Wifi.");
        goto CONNECTWIFI;
      }
    }
SETUPSERVER:
      sCommand = String("AT+CIPMUX=1");
			sendCommandToESP8226(sCommand, 2000, DEBUG);
      log("Setting up server...");
      sCommand = String("AT+CIPSERVER=1,8087");
			sResponse = sendCommandToESP8226(sCommand, 2000, DEBUG);
      if((sResponse.indexOf("OK") > -1) || sResponse.indexOf("no change") > -1)
			{
				log("Server ready.");
SETAPIP:
        //setting AP IP
        sCommand = String("AT+CIPAP=\"172.20.10.2\"");
        sResponse = sendCommandToESP8226(sCommand, 1000, DEBUG);
        if(sResponse.indexOf("OK") > -1 || sResponse.indexOf(sCommand) > -1)
        {
          log("Set AP IP Success...");
          sCommand = String("AT+CIFSR");
          sendCommandToESP8226(sCommand, 2000, DEBUG);
          bWIFIAndServerReady=true;
        }else{
         log("ERROR: Failed to setting AP IP.");
         goto SETAPIP;
        }
        
		  }else{
			  log("ERROR: Failed to set up server.");
        sCommand = String("AT+CIPSERVER=0");
        sResponse = sendCommandToESP8226(sCommand, 1000, DEBUG);
        goto SETUPSERVER;
		  }
    
  }else
  {//沒有成功連線：無法取得 OK回應。
	  log("ERROR: Failed to contact ESP8266.");
  }  
	return bWIFIAndServerReady;
}

//把 PC傳入的指令轉給 ESP8266
void proxyToESP8266()
{
  if(Serial.available())
  {
    delay(100);  // wait to let all the input command in the serial buffer    
    String sCommand = "";// read the input command in a string
    while(Serial.available())
    {
      sCommand += (char)Serial.read();
    }
    ESP8266SoftwareSerial.println(sCommand); // send the read character to the esp8266
  }
}

//加入記錄
void log(String msg)
{
	logText=logText+msg+"\n";
	if(DEBUG && msg!=""){
		Serial.println(msg);
	}
}

//把整個歷程記錄文字全部印出
void printLog()
{
	Serial.print(logText);
}

//讀取ESP8226回應
String readESP8266Response(boolean debug)
{
	String sResponse="";
  int iTimeout = 100;
  if(ESP8266SoftwareSerial.available())
  {
//    delay(100);
    long int time = millis();
    
    while( (time+iTimeout) > millis())
    {
    	while(ESP8266SoftwareSerial.available())
    	{
        char cAvailableChar = ESP8266SoftwareSerial.read();
    		sResponse += cAvailableChar;        
    	} 
    }
//   ESP8266SoftwareSerial.flush();
//   log(sResponse);
  }
  if(debug)
  {
    Serial.print(sResponse);
  }
  
  return sResponse;
}

bool closeClientConnect(int iClientID)
{
  String sResponse = sendCommandToESP8226("AT+CIPCLOSE="+ String(iClientID), 2000, DEBUG);
  delay(100);
//  if(waitForResponse(1000,"OK\r\n"))
  if(sResponse.indexOf("OK") > -1)
    return true;
  else
    return false;
}

/*
* Name: sendData
* Description: Function used to send data to ESP8266.
* Params: command - the data/command to send; timeout - the time to wait for a response; debug - print to Serial window?(true = yes, false = no)
* Returns: The response from the esp8266 (if there is a reponse)
*/
String sendData(String& command, const int timeout, boolean debug)
{
    String response = "";
    
    int dataSize = command.length();
    char data[dataSize];
    command.toCharArray(data,dataSize);
           
    ESP8266SoftwareSerial.write(data,dataSize); // send the read character to the esp8266
    if(debug)
    {
      Serial.println("\r\n====== HTTP Response From Arduino ======");
      Serial.write(data,dataSize);
      Serial.println("\r\n========================================");
    }
    
    long int time = millis();
    
    while( (time+timeout) > millis())
    {
      while(ESP8266SoftwareSerial.available())
      {
        
        // The esp has data so display its output to the serial window 
        char c = ESP8266SoftwareSerial.read(); // read the next character.
        response+=c;
      }  
    }
    
    if(debug)
    {
      Serial.print(response);
    }
    
    return response;
}
 
/*
* Name: sendHTTPResponse
* Description: Function that sends HTTP 200, HTML UTF-8 response
*/
String sendHTTPResponse(int connectionId, String& content)
{     
     // build HTTP response
//     String httpResponse;
//     String httpHeader;
//     String httpEnd;
//     // HTTP Header
//     httpHeader = "HTTP/1.1 200 OK\r\nContent-Type: text/html; charset=UTF-8\r\n";
//     httpHeader += "\r\n";
//     httpHeader += "<!DOCTYPE HTML>\r\n";
//     httpHeader += "<html>\r\n";
//     httpHeader += "<body>\r\n";
//     httpHeader += "Content-Length: ";
//     httpHeader += content.length();
//     httpHeader += "\r\n";
//     httpHeader +="Connection: close\r\n\r\n";
//     httpResponse = httpHeader + content + "\r\n</html>\r\n"; // There is a bug in this code: the last character of "content" is not sent, I cheated by adding this extra space
//     sendCIPData(connectionId,httpResponse);
//    httpEnd = "</body>\r\n";
//    httpEnd += "</html>\r\n";

//    httpResponse = "<!DOCTYPE HTML>";
//    httpResponse += "<html>";
//    httpResponse += "<body>";
//    httpResponse += "<form>";
//    httpResponse += "請填入您的無線基地台:<br>\r\n";
//    httpResponse += "SSID:<br>";
//    httpResponse += "<input type=\"text\" name=\"SSID\">";
//    httpResponse += "<br>";
//    httpResponse += "PASSWORD:<br>";
//    httpResponse += "<input type=\"text\" name=\"PASSWORD\"><br><br>";
//    httpResponse += "<input type=\"submit\" value=\"Submit\">";
//    httpResponse += "</form>";
//    httpResponse += "</body>";
//    httpResponse += "</html> ";

      char chBuffer[content.length()];
      content.toCharArray(chBuffer, content.length());
      String httpResponse;
     // HTTP Header
//     httpResponse = "HTTP/1.1 200 OK\r\nContent-Type: text/html; charset=UTF-8\r\n"; 
//     httpResponse += "Content-Length: ";
//     httpResponse += content.length();
//     httpResponse += "\r\n";
//     httpResponse +="Connection: close\r\n\r\n";
       httpResponse = "<!DOCTYPE HTML>";
       httpResponse += "<html>";
       httpResponse += "<body>";
       httpResponse += chBuffer;
       httpResponse += "</body>";
       httpResponse += "</html> ";
//     String httpResponse = httpHeader;
//
//     Serial.print("\nDEBUG sendHTTPResponse httpHeader: ");
//     Serial.println(httpHeader);
     Serial.print("\nDEBUG sendHTTPResponse content: ");
     Serial.println(chBuffer);
     Serial.print("\nDEBUG sendHTTPResponse httpResponse: ");
     Serial.println(httpResponse);
    
     String sResponse = sendCIPData(connectionId, httpResponse);

     return sResponse;
}
 
/*
* Name: sendCIPDATA
* Description: sends a CIPSEND=<connectionId>,<data> command
*
*/
String sendCIPData(int connectionId, String& data)
{
   Serial.print("\nDEBUG sendCIPData data: ");
   Serial.println(data);
     
   String cipSend = "AT+CIPSEND=";
   cipSend += connectionId;
   cipSend += ",";
   cipSend +=data.length();
//   cipSend +="\r\n";
   String sResponse;
   sResponse = sendCommandToESP8226(cipSend,1000,DEBUG);
   sResponse = sendData(data, 1000, DEBUG);

   return sResponse;
}

/*
* Name: sendCommand
* Description: Function used to send data to ESP8266.
* Params: command - the data/command to send; timeout - the time to wait for a response; debug - print to Serial window?(true = yes, false = no)
* Returns: The response from the esp8266 (if there is a reponse)
*/
String sendCommandToESP8226(String& command, const int timeout, boolean debug)
{
    String response = "";
    String sCommandd = command+"\r\n";
    ESP8266SoftwareSerial.print(sCommandd); // send the read character to the esp8266
    
    long int time = millis();
    
    while( (time+timeout) > millis())
    {
      while(ESP8266SoftwareSerial.available())
      {
        
        // The esp has data so display its output to the serial window 
        char c = ESP8266SoftwareSerial.read(); // read the next character.
        response+=c;
      }  
      ESP8266SoftwareSerial.flush();
    }
    
    if(debug)
    {
      Serial.print(command);
      Serial.print(" sendCommandToESP8226 Response: \n");
      Serial.print(response);
    }    
    
    return response;
}
 
